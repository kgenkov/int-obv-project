class Feed {
    constructor (config) {
        this.limit = config.limit;
        this.timeout = config.timeout;
        this.feedNode = config.feedNode;
        this.loaderNode = config.loaderNode;
        this.offset = -this.limit;
        this.url = '';

        // initialize feed
        this.init();
    }


    createNode (options) {
        const node = document.createElement(options.type);
        node.innerText = options.text ? options.text : '';
        node.setAttribute('class', options.className);
    
        return node;
    }


    createPost (data) {
        const post = this.createNode({ type: 'div', className: 'main__post' });
        const paragraph = this.createNode({ type: 'p', className: 'main__post-paragraph', text: data.name });
        post.appendChild(paragraph);

        return post;
    }


    async fetchData () {
        const response = await fetch(`https://pokeapi.co/api/v2/pokemon?limit=${this.limit}&offset=${this.offset += this.limit}`);
        const data = await response.json();

        return data;
    }


    async populateFeed (callback) {
        const data = await this.fetchData();
        for (let i = 0; i < data.results.length; i++) {
            const post = this.createPost(data.results[i]);
            this.feedNode.insertBefore(post, this.loaderNode); // append posts before the loaderNode node
        }
        if (typeof callback === 'function') callback(); // callback function is called after the data is fetched and dom is populated with posts
    }

    
    populateFeedOnTrigger () {
        const trigger = this.loaderNode;
        const spinnerObserver = new IntersectionObserver((entries) => {
            const intersector = entries[0];
            if (intersector.isIntersecting) setTimeout(() => this.populateFeed(), this.timeout);
        }, {});
        spinnerObserver.observe(trigger);
    }


    init () {
        this.populateFeed(() => this.populateFeedOnTrigger());
    }
}