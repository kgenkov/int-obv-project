const feedNode = document.getElementById('main-feed');
const loaderNode = document.getElementById('spinner');

const LOADER_TIMEOUT = 800; // ms
const POSTS_PER_REQUEST = 15;

const feed = new Feed({
    limit: POSTS_PER_REQUEST,
    timeout: LOADER_TIMEOUT,
    feedNode: feedNode,
    loaderNode: loaderNode
});