const backgroundNode = document.getElementById('main-background');
const advertismentNode = document.getElementById('main-ad');

// trigger sticky state on advertisment
((element, trigger) => {
    const observer = new IntersectionObserver((entries) => {
        const intersector = entries[0];
        !intersector.isIntersecting ? element.classList.add('fixed') : element.classList.remove('fixed');
    }, {});
    observer.observe(trigger);
})(advertismentNode, backgroundNode);